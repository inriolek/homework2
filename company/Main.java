package com.company;

import com.company.details.Engine;
import com.company.entities.Person;
import com.company.professions.Driver;
import com.company.vehicles.Car;
import com.company.vehicles.Lorry;
import com.company.vehicles.SportCar;

public class Main {
    public static void main(String[] args) {
        Car benz = new Car("Benz","V8","2500kg");
        Person pers = new Person("Alex","Kovalchuk",35,603540133);
        Engine eng = new Engine("Mercedes Benz Germany cop.","500PS");
        Driver dri = new Driver();
        Lorry lor = new Lorry();
        SportCar skor = new SportCar();
        System.out.println(pers);
        System.out.println(dri);
        System.out.println(benz);
        System.out.println(eng);
        benz.start();
        benz.stop();
        benz.turnLeft();
        benz.turnRight();
        System.out.println("--------------------");
        System.out.println(lor);
        lor.upWeight();
        System.out.println("---------------------");
        System.out.println(skor);
        skor.getSpeed();


    }
}
