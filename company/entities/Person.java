package com.company.entities;

public class Person {
    String firstName;
    String lastName;
    int age;
    int phoneNumber;
    public String experienceCar;

    public Person(String firstName, String lastName, int age, int phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.phoneNumber = phoneNumber;
        }

    public Person(String experienceCar) {
       this.experienceCar = experienceCar;
    }

    public String toString(){
        return firstName + " " + lastName + " " + age + " " + phoneNumber ;}
    }
