package com.company.vehicles;

public class Lorry extends Car {

    public Lorry() {
        super("HUMMER" + " ","4x4" + " ","5000KG");
    }

        public void upWeight(){
        System.out.println("Lorry can get up more weight");
    }
    public  String toString(){
        return super.engine + super.model + super.weight;
    }
}
