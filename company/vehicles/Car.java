package com.company.vehicles;

public class Car {
    String model;
    String engine;
    String weight;
    public Car(String model, String engine, String weight){

        this.model = model;
        this.engine = engine;
        this.weight= weight;

    }

    public void start(){
        System.out.println("Lets Goo");
    }
    public void stop(){
        System.out.println("To Stop");
    }
    public void turnRight(){
        System.out.println("Make Turn to right side");
    }
    public void turnLeft(){
        System.out.println("Make Turn to left side");
    }
    public String toString(){
        return model + " " + engine + " " + weight + " ";
    }

}
