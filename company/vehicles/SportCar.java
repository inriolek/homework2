package com.company.vehicles;

public class SportCar extends Car {
    public SportCar (){
        super("Mazeratti"+ " ","v12"+ " ","2000KG");
    }
    public void getSpeed(){
        System.out.println("Max speed is : 300km/h");
    }
    public String toString(){
        return super.model + super.engine + super.weight;
    }
}
